from __future__ import absolute_import, division, print_function

import logging
import os
import json
import time
from collections import defaultdict

import numpy as np
import pandas as pd
import torch
import transformers

from opt import get_args
from modeling import (evaluate, read_in, load_tokenizer, set_seed)


transformers.logging.set_verbosity_error()
logger = logging.getLogger(__name__)


def main():
    start_time = time.time()
    args = get_args()

    # Setup logging
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %H:%S', level=logging.INFO)

    # Setup CUDA, GPU & distributed training
    if not args.force_cpu and not torch.cuda.is_available():
        logger.info("NO available GPU. STOPPED. If you want to continue without GPU, add --force_cpu")
        return

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    args.n_gpu = torch.cuda.device_count()
    args.device = device

    logger.warning("device: %s, n_gpu: %s", device, args.n_gpu)

    if not args.config_name_or_path:
        config_file_name = f"./config/{args.model_type}.json"
        assert os.path.exists(config_file_name), "requested BERT model variant not in the preset. You can place the corresponding config file under the folder /config/"
        args.config_name_or_path = config_file_name

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    # prepare model
    config = transformers.BertConfig.from_pretrained(args.config_name_or_path, num_labels=args.num_labels)
    tokenizer = load_tokenizer(args.pretrained_model_path, args.model_type)

    model_dir = os.path.join(args.finetuned_model_path, args.model_type)
    test_dataloader = read_in(args, tokenizer, inference=True)
    id2label = json.load(open(os.path.join(args.finetuned_model_path, "id2label.json"), 'r'))
    id2label = {int(k): v for k, v in id2label.items()}

    result = defaultdict(list)
    # Evaluate the best model on Test set
    if args.ensemble_models:
        ensemble_models0 = list((int(s.strip()) - 1) for s in args.ensemble_models.split(','))
    elif args.num_ensemble:
        ensemble_models0 = list(range(args.num_ensemble))
    else:
        raise RuntimeError()
    ensemble_models1 = list(ne + 1 for ne in ensemble_models0)
    for ne in ensemble_models0:
        torch.cuda.empty_cache()

        set_seed(args, ne)

        ensemble_model_path = os.path.join(args.finetuned_model_path, f"{args.model_type}/ensemble_{ne+1}")
        model = transformers.BertForSequenceClassification.from_pretrained(ensemble_model_path, config=config, output_loading_info=False)
        model.to(args.device)

        pred = evaluate(test_dataloader, model, predict_only=True)
        result[ne + 1].append(pred)

    logger.info('id2label = ' + str(id2label))
    for ne in ensemble_models1:
        probs = np.concatenate(result[ne])
        logger.info('probs = ' + str(probs))
        df = pd.DataFrame({id2label[lid]: probs[:, lid] for lid in sorted(id2label.keys())})
        df.to_csv(os.path.join(args.output_dir, f"probas_ensemble_{ne}.csv"), index=False)

    end_time = time.time()
    logger.info(f"time consumed (prediction):{(end_time-start_time):.3f} s.")
    logger.info("prediction done!")

if __name__ == "__main__":
    main()


# python3 eval.py --data_dir data/test.csv --output_dir prediction --model_type biobert --pretrained_model_path pretrained_model --finetuned_model_path finetuned_model --num_labels 2 --num_ensemble 1 --force_cpu
