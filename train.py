from __future__ import absolute_import, division, print_function

import logging
import os
import random
import time

import numpy as np
import pandas as pd
import torch
import transformers 
from transformers import BertConfig

from tqdm import tqdm, trange
from sklearn.metrics import f1_score
from opt import get_args
from loader import DataLoader
from modeling import (train, evaluate, read_in, load_model, load_tokenizer, set_seed)

transformers.logging.set_verbosity_error()
logger = logging.getLogger(__name__)

def main():
    start_time = time.time()
    args = get_args()
    
    # Setup CUDA, GPU & distributed training
    if not args.force_cpu and not torch.cuda.is_available():
        logger.info("NO available GPU. STOPPED. If you want to continue without GPU, add --force_cpu")
        return 

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    args.n_gpu = torch.cuda.device_count()
    args.device = device

    # Setup logging
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',datefmt='%m/%d/%Y %H:%S',level=logging.INFO,filename="training_log",filemode='w')
    logger.warning("device: %s, n_gpu: %s",device, args.n_gpu)

    if not args.config_name_or_path:
        config_file_name = f"./config/{args.model_type}.json"
        assert os.path.exists(config_file_name), "requested BERT model variant not in the preset. You can place the corresponding config file under the folder /config/"
        args.config_name_or_path = config_file_name

    config = BertConfig.from_pretrained(args.config_name_or_path,
                                        num_labels=args.num_labels)
    tokenizer = load_tokenizer(args.pretrained_model_path,args.model_type)

    output_dir = os.path.join(args.finetuned_model_path,args.model_type,"training_record")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    train_dataloader, dev_dataloader = read_in(args,tokenizer)

    all_dev_loss, all_dev_score, all_best_epochs = [], [], []
    # Evaluate the best model on Test set
    for ne in range(args.num_ensemble):
        logger.info(f"<<<<<<<<<<  ensemble_{ne+1}  >>>>>>>>>>>")
        torch.cuda.empty_cache()
        
        set_seed(args,ne)
        model = load_model(args.pretrained_model_path,args.model_type,config=config,output_loading_info=False)
        model.to(args.device)

        nb_epochs, loss_record, score_record, best_epoch = train(args,train_dataloader,dev_dataloader,model,config,ne+1)
        
        all_best_epochs.append(best_epoch)
        all_dev_loss.append(loss_record+[float("NaN")]*(nb_epochs-len(loss_record)))
        all_dev_score.append(score_record+[float("NaN")]*(nb_epochs-len(score_record)))
        
    all_dev_loss = np.array(all_dev_loss)
    all_dev_score = np.array(all_dev_score)
    
    df_loss_record = pd.DataFrame({"members":[f"ensemble_{i}" for i in range(1,args.num_ensemble+1)],**{f"epoch_{i}":all_dev_loss[:,i] for i in range(nb_epochs)}})
    df_loss_record.to_csv(os.path.join(output_dir,"training_record_loss.csv"),index=False)

    df_score_record = pd.DataFrame({"members":[f"ensemble_{i}" for i in range(1,args.num_ensemble+1)],**{f"epoch_{i}":all_dev_score[:,i] for i in range(nb_epochs)}})
    df_score_record.to_csv(os.path.join(output_dir,"training_record_score.csv"),index=False)

    df_best_epochs = pd.DataFrame({"members":[f"ensemble_{i}" for i in range(1,args.num_ensemble+1)],"best_epoch":all_best_epochs})
    df_best_epochs.to_csv(os.path.join(output_dir,"training_best_epochs.csv"),index=False)
    
    end_time = time.time()
    logger.info(f"time consumed (training): {(end_time-start_time):.3f} s.")
    logger.info("training record saved.")
    
if __name__ == "__main__":
    main()
