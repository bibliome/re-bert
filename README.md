## Relation Extraction with different types of BERT

### Functions
- Three types of BERT in preset: use --model_type {bert,biobert,scibert}. It is possible to add BERT models not in the preset list, but the corresponding configuration file needs to be placed under the folder /config/. 
- If no validation data provided, a part (ratio set by the option --train_dev_split) of training data will be randomly split as the validation data.
- It is possible to run without GPU, set --force_cpu
- "no_relation" should be counted for the option --num_labels 
- The output files contain probabilities for each type of relation, write your own vote function to get the final result. 

### Usage
The example data contains files from BB-rel 2019 task. Add the option --debug for a quick test.

##### training
python3 train.py --data_dir ./data/rebert/train_dev.csv --train_dev_split 0.15 --model_type biobert --pretrained_model_path ./pretrained_model/ --finetuned_model_path ./finetuned_model/  --num_labels 2 --num_ensemble 3  --early_stopping --batch_size 16 --no_randomness

##### inference only
python3 eval.py --data_dir ./data/rebert/test.csv --output_dir ./prediction/ --model_type biobert --pretrained_model_path ./pretrained_model/ --finetuned_model_path ./finetuned_model/ --num_labels 2 --num_ensemble 3

##### pre-finetuned model on BB-rel 19 corpus

download at (~6.5G):

https://drive.google.com/file/d/14K0DIRqFNu705HZFRcgY1hFzbqZg2o-s/view?usp=sharing

score of the generated voting result (using the evaluation tool of BB-rel 19):
    
    Lives_In and Exhibits

      F1: 0.7017543859649122

      Recall: 0.6674082313681868

      Precision: 0.7398273736128237
      
      Predictions: 811
