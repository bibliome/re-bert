import os
import numpy as np
import pandas as pd
import logging
import torch

bbrel_arg_to_rel = {"Property":"Exhibits","Location":"Lives_In"}

logger = logging.getLogger(__name__)

class DataLoader(object):
    def __init__(self,args,df,tokenizer,label2id,tag,eval=False,inference=False):
        self.tokenizer = tokenizer
        self.max_len = args.max_seq_length
        self.inference = inference
        self.device = args.device
        
        first_sents, second_sents = df.sentence_1.values, df.sentence_2.values
        no_second_sents = df.sentence_2.isnull().values
        
        data = []
        for s1, s2,e in zip(first_sents,second_sents,no_second_sents):
            if e:
                data.append(s1)
            else:
                data.append([s1,s2])
        
        if not inference:
            labels = [label2id.get(l,0) for l in df.label.values]
        
        # shuffle the data for training set
        
        if not inference:
            data = list(zip(data,labels))
            if not eval:
                indices = list(range(len(df)))
                np.random.shuffle(indices)
                data = [data[i] for i in indices]

        data = [data[i:i+args.batch_size] for i in range(0,len(data),args.batch_size)]
        self.data = data
        logger.info(f"{tag}: {len(data)} batches generated.")

    def __len__(self):
        return len(self.data)

    def __getitem__(self,key):
        if not isinstance(key,int):
            raise TypeError
        if key < 0 or key >= len(self.data):
            raise IndexError
        batch = self.data[key]

        if not self.inference:
            batch_sents, batch_labels = map(list,zip(*batch))
        else:
            batch_sents = batch
        #print(batch_sents) 
        encoding = self.tokenizer(batch_sents, add_special_tokens = True,    
                                              truncation = True, 
                                              max_length = self.max_len,
                                              padding = "max_length", 
                                              return_attention_mask = True, 
                                              return_tensors = "pt")

        encoding =  {k:v.to(self.device) for k, v in encoding.items()}

        if not self.inference:
            encoding.update({"labels":torch.Tensor(batch_labels).long().to(self.device)})
        return encoding

    def __iter__(self):
        for i in range(self.__len__()):
            yield self.__getitem__(i)



